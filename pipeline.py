import math
from sklearn import datasets
import numpy as np
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.cluster import KMeans
from sklearn.metrics import davies_bouldin_score

np.seterr(divide='ignore', invalid='ignore')

class Hk(object):
  def __init__(self,csv_file):
    self.table  = self.read_csv(csv_file)
    self.index  = self.table.pop(0)
    self.data   = self.get_data(self.table)
    self.genes  = self.get_genes(self.table)
    self.empty  = []

  def read_csv(self, filename):
    x = []
    with open(filename) as csv:
      for line in csv:
        x.append(line.strip().split(','))

      return x

  def get_data(self,table):
    data = []

    l = []
    for line in table:
      for e in line[1:]:
          l.append(float(e))
      data.append(l)
      l = []

    return data

  def get_genes(self, table):
    genes = []
    for e in table:
      genes.append(e[0])

    return genes

  def remove_element(self, index):
    gene = [self.genes.pop(index)]
    data = self.data.pop(index)
    gene.extend(data)
    return gene

  def remove_empty_elements(self, data):
    for i in range(len(data)):
      if sum(data[i]) == 0:
        self.empty.append(self.remove_element(i))

  def normalization(self, data):
    for l in range(len(data)):
      for i in range(len(data[l])):
        if data[l][i]:
          data[l][i] = math.log(data[l][i]) + 1

    return data

  def bouldin_score(self, data, k):
    X = np.array(data)
    kmeans = KMeans(n_clusters=k, random_state=1).fit(X)
    labels = kmeans.labels_
    score = davies_bouldin_score(X, labels)
    return score

  def score_silhouette(self, data, k):
    X = np.array(data)
    kmeans_model = KMeans(n_clusters=k, random_state=1).fit(X)
    labels = kmeans_model.labels_
    score = metrics.silhouette_score(X, labels, metric='euclidean')
    return score


  def call_silhouette(self, data, n):
    score_old = -200
    k = 2
    for i in range(n):
        score = self.score_silhouette(data, i + 2)
        score_old = self.best_score(score, score_old)
        if score == score_old:
            k = i + 2

    return k, score_old


  def call_bouldin(self, data, n):
    score_old = -200
    k = 2
    for i in range(n):
        score = self.bouldin_score(data, i + 2)
        score_old = self.best_score(score, score_old)
        if score == score_old:
            k = i + 2

    return k, score_old


  def best_score(self, a, b):
    score = sorted([a - 1, b - 1])
    if (a > 1 and b < 1) or (b > 1 and a < 1):
        return(score[-1] + 1)
    if a > 1 and b > 1:
        return(score[0] + 1)
    else:
        return((score[-1] + 1))

    
    
  def average_cluster(self, data, k):
    average = self.call_bouldin(data, k) + self.call_silhouette(data, k)
    av = average[0] + average[2]
    x = av/2
    return x
    

hk = Hk('data.csv')
hk.remove_empty_elements(hk.data)
hk.data = hk.normalization(hk.data)
#hk.bouldin_score(hk.data, 2)
#hk.score_silhouette(hk.data, 2)
a = hk.call_bouldin(hk.data, 100)
b = hk.call_silhouette(hk.data, 100)
print(a, b)
print(hk.average_cluster(hk.data, 100))
